// ============== Class Person as Paren ================
// Person can greeting|  greeting()

class Person {
  constructor(name) {
    this.name = name;
  }

  greeting() {
    console.log(`Hello my name ${this.name}`);
  }
}

// ============== Class Owner inherit Person ================
// Owner can add new Employee |  addEmployee()
// Owner can fire employee | fireEmployee()
// Owner can get list employee | getListEmployee()

class Owner extends Person {
  constructor(name) {
    super(name);
    this.listEmployee = [];
  }

  addEmployee(name) {
    this.listEmployee.push(name);
  }

  fireEmployee(name) {
    let nameEmployee = name;

    let filteredEmployee = this.listEmployee.filter(
      (item) => !nameEmployee.includes(item)
    );

    this.listEmployee = filteredEmployee;
  }

  listEmployee() {
    console.log(`all employee ${this.listEmployee.toString()}`);
  }
}

// ============== Class Employee inherit Person ================
// Employee can refill; Gasoline |  refillGasoline('customer', 4 , 'nameStation')
//      -if total gasoline < purchasedFuel Employee should ask the customer to
//       to fill in another gate and employee make refile station gasoline.
//      -if customer have member customer get discount refile price.
// Employee can activate member customer | activateMember()
// Employee can deactivate member customer | deactivateMember()

class Employee extends Person {
  constructor(name) {
    super(name);
  }

  refillGasoline(purchasedFuel, refilePrice) {
    // Check if total gasoline in station have more fuels than purchased fuel by customer
    if (Station.totalGasoline > purchasedFuel) {
      Station.setTotalGasoline(purchasedFuel); 
      Customer.setCountTransaction(1);  
      
        if(Customer.memberStatus){
            if(refilePrice >= 100.000){
                refilePrice -= refilePrice*0.1 
            } 
                                              
            else if( refilePrice >= 20.000){ 
                refilePrice -= refilePrice*0.025
            }
        } 

    }
    else {  
        Station.setStationNumber(1) 
        Customer.setCountTransaction(1);
        this.refillGasolineStatus(100) 
    } 
    Customer.setRemainFuels(purchasedFuel) 
  }
  
  refillGasolineStatus(refillGasoline ) {  
      Station.setTotalGasoline = refillGasoline 
      Station.setStationNumber(-1) 
   
  }

  activateMember() { 
    if (Costumer.countTransaction >= 3) {
      Costumer.setMemberStatus(true);
    } else {
      console.log('You don\'t meet the minimum requirements at least 3 transactions');
    }
  }

  deactivateMember() {
    Costumer.setMemberStatus(false);
  }
}

// ============== Class Station ================
// Station have attribute gasoline

class Station {
  constructor() {
    this.stationNumber = 1;
    this.totalGasoline = 100;
  }

  setStationNumber(stationNumber){
      this.stationNumber += stationNumber;
  }

  setTotalGasoline(gasoline) {
    this.totalGasoline -= gasoline;
  }

  changeGasoline()
}

// ============== Class Customer ===============
// Customer must have maximal tank capacity
// Customer can ride as long as their tank is enough, method getTotalFuelSpent(distance)
// 1 Liter of gasoline can go for 5 km
class Customer extends Person {
  constructor(name, tankCapacity, remainFuels) { 
    super(name); 
    this.remainFuels = remainFuels; 
    this.tankCapacity = tankCapacity; 
    this.memberStatus = false;
    this.countTransaction = 0;
  } 
  getTotalFuelSpent(distance) { 
    console.log(`Jarak yang ditempuh sebesar ${distance} ,  
    must have  ${distance * 0.2} liter`);
  } 

  getMemberStatus() { 
    console.log(`Member status is ${this.memberStatus}`);
  }

  getCountTransaction() {
    console.log(`Total Transaction is ${this.countTransaction}`);
  } 

  setMemberStatus(memberStatus) {
    this.memberStatus = memberStatus;
  }
  setCountTransaction(count) {
    this.countTransaction += count;
  }

  getCountTransaction() {
    console.log(`Total Transaction is ${this.countTransaction}`);
  }

  setRemainFuels(remainFuels){
    this.remainFuels += remainFuels
  }
}
